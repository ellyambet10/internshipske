# frozen_string_literal: true

class CreateOrganisations < ActiveRecord::Migration[5.2]
  def change
    create_table :organisations do |t|
      t.string :category
      t.string :name
      t.text :body

      t.timestamps
    end
  end
end
