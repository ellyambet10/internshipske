# frozen_string_literal: true

class AddAttachmentImageToInternships < ActiveRecord::Migration[5.2]
  def self.up
    change_table :internships do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :internships, :image
  end
end
