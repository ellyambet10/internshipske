# frozen_string_literal: true

class CreateInternships < ActiveRecord::Migration[5.2]
  def change
    create_table :internships do |t|
      t.string :category
      t.string :title
      t.text :body
      t.string :url
      t.references :organisation, foreign_key: true

      t.timestamps
    end
  end
end
