# frozen_string_literal: true

class AddAttachmentImageToOrganisations < ActiveRecord::Migration[5.2]
  def self.up
    change_table :organisations do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :organisations, :image
  end
end
