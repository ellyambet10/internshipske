# frozen_string_literal: true

class CreateHomes < ActiveRecord::Migration[5.2]
  def change
    create_table :homes do |t|
      t.string :title
      t.text :body
      t.string :advert
      t.string :testimonial

      t.timestamps
    end
  end
end
