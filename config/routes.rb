# frozen_string_literal: true

Rails.application.routes.draw do
  resources :subscriptions
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users
  resources :blogs
  resources :abouts
  resources :contacts
  resources :internships
  resources :organisations
  resources :homes

  root 'homes#index'
end
