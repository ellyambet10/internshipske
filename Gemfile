# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '2.6.3'

gem 'pg', '>= 0.18', '< 2.0', group: :production # Added postgres and made it production only
gem 'rails_12factor', group: :production
gem 'letsencrypt-rails-heroku', group: 'production'

gem 'puma', '~> 3.11'
gem 'rails', '~> 5.2.3'
gem 'sass-rails', '~> 5.0'
gem 'uglifier', '>= 1.3.0'
gem 'mini_racer', platforms: :ruby
gem 'coffee-rails', '~> 4.2'
gem 'jbuilder', '~> 2.5'
gem 'turbolinks', '~> 5'
# gem 'redis', '~> 4.0'
# gem 'bcrypt', '~> 3.1.7'
# gem 'mini_magick', '~> 4.8'
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  gem 'byebug', platforms: %i[mri mingw x64_mingw]
  gem 'capistrano', '~> 3.11.0'
  gem 'capistrano-rails', '~> 1.4.0'
  gem 'capistrano-passenger', '~> 0.2.0'
  gem 'capistrano-bundler', '~> 1.1', require: false
  # gem 'capistrano-rvm', require: false
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
  gem 'web-console', '>= 3.3.0'
end

group :test do
  gem 'capybara', '>= 2.15'
  gem 'chromedriver-helper'
  gem 'selenium-webdriver'
end

gem 'tzinfo-data', platforms: %i[mingw mswin x64_mingw jruby]

gem 'activeadmin'
gem 'devise', '~> 4.2'
gem 'inherited_resources'
gem 'mimemagic', '~> 0.3.0'
gem 'paperclip', '~> 6.0.0'
gem 'rack-cors'
gem 'simple_form'

gem 'active_skin'
gem 'arctic_admin'
gem 'bootstrap-sass'

gem 'pagy'
gem 'msgpack', '~> 1.2.10'

gem 'webpacker', '>= 4.0.0.rc.3'

