# frozen_string_literal: true

class SubscriptionsController < InheritedResources::Base
  before_action :set_subscription, only: %i[show edit update destroy]

  def index
    @subscriptions = Subscription.all
  end

  def new
    @subscription = Subscription.new
  end

  def create
    @subscription = Subscription.new(subscription_params)

    respond_to do |format|
      if @subscription.save
        format.html do
          flash[:success] = 'Subscribed!'
          redirect_to subscriptions_url
        end
      else
        format.html { flash[:failure!] = 'Not Subscribed! Verify Details & Try Again!' }
        format.html { render 'new' }
      end
    end
  end

  private

  def set_contact
    @subscription = Subscription.find(params[:id])
  end

  def subscription_params
    params.require(:subscription).permit(:email)
  end
end
