# frozen_string_literal: true

class InternshipsController < ApplicationController
  before_action :set_internship, only: %i[show edit update destroy]

  def index
    @pagy, @internships = pagy(Internship.all.order('created_at DESC'), items: 4)
    @search = params['search']

    if @search.present?
      @category = @search['category']
      @internships = Internship.where('category ILIKE ?', "%#{@category}%")
    end
  end

  def show
    @internship = Internship.find(params[:id])
    @internships = Internship.order('created_at desc').limit(4).offset(1)
  end

  def new
    @internship = Internship.new
  end

  def edit; end

  def create
    @internship = Internship.new(internship_params)

    respond_to do |format|
      if @internship.save
        format.html { redirect_to @internship, notice: 'Internship was successfully created.' }
        format.json { render :show, status: :created, location: @internship }
      else
        format.html { render :new }
        format.json { render json: @internship.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @internship.update(internship_params)
        format.html { redirect_to @internship, notice: 'Internship was successfully updated.' }
        format.json { render :show, status: :ok, location: @internship }
      else
        format.html { render :edit }
        format.json { render json: @internship.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @internship.destroy
    respond_to do |format|
      format.html { redirect_to internships_url, notice: 'Internship was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

  def set_internship
    @internship = Internship.find(params[:id])
  end

  def internship_params
    params.require(:internship).permit(:category, :title, :body, :url, :organisation_id)
  end
end
