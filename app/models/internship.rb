# frozen_string_literal: true

class Internship < ApplicationRecord
  belongs_to :organisation

  validates :category, presence: true
  validates :title, presence: true, length: { minimum: 5 }
  validates :body, presence: true
  validates :url, presence: true

  has_attached_file :image, styles: { medium: '300x300>', thumb: '100x100>' }
  validates_attachment_content_type :image, content_type: %r{\Aimage/.*\z}
end
