# frozen_string_literal: true

class Organisation < ApplicationRecord
  has_many :internships, dependent: :destroy

  validates :category, presence: true
  validates :name, presence: true
  validates :body, presence: true

  has_attached_file :image, styles: { medium: '300x300>', thumb: '100x100>' }
  validates_attachment_content_type :image, content_type: %r{\Aimage/.*\z}
end
