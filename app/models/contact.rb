# frozen_string_literal: true

class Contact < ApplicationRecord
  validates :username, presence: true
  validates :email, presence: true, uniqueness: true
  validates :message, presence: true
end
