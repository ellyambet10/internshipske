# frozen_string_literal: true

json.extract! internship, :id, :category, :title, :body, :organisation_id, :created_at, :updated_at
json.url internship_url(internship, format: :json)
