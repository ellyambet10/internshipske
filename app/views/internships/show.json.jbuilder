# frozen_string_literal: true

json.partial! 'internships/internship', internship: @internship
