# frozen_string_literal: true

json.array! @internships, partial: 'internships/internship', as: :internship
