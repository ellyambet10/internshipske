# frozen_string_literal: true

json.extract! about, :id, :title, :body, :faq, :created_at, :updated_at
json.url about_url(about, format: :json)
