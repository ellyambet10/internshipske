# frozen_string_literal: true

json.extract! home, :id, :title, :body, :advert, :testimonial, :created_at, :updated_at
json.url home_url(home, format: :json)
