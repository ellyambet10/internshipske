# frozen_string_literal: true

ActiveAdmin.register Blog do
  permit_params :title, :body, :image

  show do |_t|
    attributes_table do
      row :title
      row :body

      row :image do
        blog.image? ? image_tag(blog.image.url, height: '100') : content_tag(:span, 'No photo yet')
      end
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :title
      f.input :body

      f.input :image, hint: f.blog.image? ? image_tag(blog.image.url, height: '100') : content_tag(:span, 'Upload JPG/PNG/GIF image')
    end
    f.actions
  end
end
