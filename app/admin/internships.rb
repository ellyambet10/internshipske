# frozen_string_literal: true

ActiveAdmin.register Internship do
  permit_params :category, :title, :body, :url, :organisation_id, :image

  show do |_t|
    attributes_table do
      row :category
      row :title
      row :body
      row :url
      row :organisation_id

      row :image do
        internship.image? ? image_tag(internship.image.url, height: '100') : content_tag(:span, 'No photo yet')
      end
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :category
      f.input :title
      f.input :body
      f.input :url
      f.input :organisation_id

      f.input :image, hint: f.internship.image? ? image_tag(internship.image.url, height: '100') : content_tag(:span, 'Upload JPG/PNG/GIF image')
    end
    f.actions
  end
end
