# frozen_string_literal: true

ActiveAdmin.register Home do
  permit_params :title, :body, :advert, :testimonial, :image

  show do |_t|
    attributes_table do
      row :title
      row :body
      row :advert
      row :testimonial

      row :image do
        home.image? ? image_tag(home.image.url, height: '100') : content_tag(:span, 'No photo yet')
      end
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :title
      f.input :body
      f.input :advert
      f.input :testimonial

      f.input :image, hint: f.home.image? ? image_tag(home.image.url, height: '100') : content_tag(:span, 'Upload JPG/PNG/GIF image')
    end
    f.actions
  end
end
