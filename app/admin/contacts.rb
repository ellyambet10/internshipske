# frozen_string_literal: true

ActiveAdmin.register Contact do
  permit_params :username, :email, :message

  show do |_t|
    attributes_table do
      row :username
      row :email
      row :message
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :username
      f.input :email
      f.input :message
    end
    f.actions
  end
end
