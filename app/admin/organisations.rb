# frozen_string_literal: true

ActiveAdmin.register Organisation do
  permit_params :category, :name, :position, :body, :image

  show do |_t|
    attributes_table do
      row :category
      row :name
      row :body

      row :image do
        organisation.image? ? image_tag(organisation.image.url, height: '100') : content_tag(:span, 'No photo yet')
      end
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :category
      f.input :name
      f.input :body

      f.input :image, hint: f.organisation.image? ? image_tag(organisation.image.url, height: '100') : content_tag(:span, 'Upload JPG/PNG/GIF image')
    end
    f.actions
  end
end
