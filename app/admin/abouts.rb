# frozen_string_literal: true

ActiveAdmin.register About do
  permit_params :title, :body, :faq, :image

  show do |_t|
    attributes_table do
      row :title
      row :body
      row :faq

      row :image do
        about.image? ? image_tag(about.image.url, height: '100') : content_tag(:span, 'No photo yet')
      end
    end
  end

  form html: { enctype: 'multipart/form-data' } do |f|
    f.inputs do
      f.input :title
      f.input :body
      f.input :faq

      f.input :image, hint: f.about.image? ? image_tag(about.image.url, height: '100') : content_tag(:span, 'Upload JPG/PNG/GIF image')
    end
    f.actions
  end
end
