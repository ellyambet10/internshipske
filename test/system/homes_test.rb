# frozen_string_literal: true

require 'application_system_test_case'

class HomesTest < ApplicationSystemTestCase
  setup do
    @home = homes(:one)
  end

  test 'visiting the index' do
    visit homes_url
    assert_selector 'h1', text: 'Homes'
  end

  test 'creating a Home' do
    visit homes_url
    click_on 'New Home'

    fill_in 'Advert', with: @home.advert
    fill_in 'Body', with: @home.body
    fill_in 'Testimonial', with: @home.testimonial
    fill_in 'Title', with: @home.title
    click_on 'Create Home'

    assert_text 'Home was successfully created'
    click_on 'Back'
  end

  test 'updating a Home' do
    visit homes_url
    click_on 'Edit', match: :first

    fill_in 'Advert', with: @home.advert
    fill_in 'Body', with: @home.body
    fill_in 'Testimonial', with: @home.testimonial
    fill_in 'Title', with: @home.title
    click_on 'Update Home'

    assert_text 'Home was successfully updated'
    click_on 'Back'
  end

  test 'destroying a Home' do
    visit homes_url
    page.accept_confirm do
      click_on 'Destroy', match: :first
    end

    assert_text 'Home was successfully destroyed'
  end
end
